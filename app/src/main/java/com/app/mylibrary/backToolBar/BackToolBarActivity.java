package com.app.mylibrary.backToolBar;

import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.app.mylibrary.R;

public abstract class BackToolBarActivity extends BaseToolBarActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void configureActionBar(ActionBar actionBar) {
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    public static void applyBackIconToBar(@Nullable ActionBar actionBar, @Nullable Toolbar toolbar){
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        }
    }

    @Override
    protected final void manageToolbar() {
        super.manageToolbar();
        applyBackIconToBar(getSupportActionBar(), toolbar);
    }
}
