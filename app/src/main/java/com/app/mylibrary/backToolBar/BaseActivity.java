package com.app.mylibrary.backToolBar;

import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    protected boolean isAliveActivity(){
        if(isFinishing()){
            return false;
        }
        return !isDestroyed();
    }

}
