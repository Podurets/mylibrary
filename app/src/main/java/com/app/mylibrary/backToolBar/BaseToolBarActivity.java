package com.app.mylibrary.backToolBar;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.app.mylibrary.R;


public abstract class BaseToolBarActivity extends BaseActivity {

    protected Toolbar toolbar;

    protected FrameLayout contentContainer;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.base_toolbar_layout);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        initViews(window.getDecorView());
    }


    private void initViews(View rootView) {
        contentContainer =  rootView.findViewById(R.id.contentContainer);
        toolbar = rootView.findViewById(R.id.toolBarView);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            configureActionBar(actionBar);
        }
        manageToolbar();
    }

    protected void manageToolbar(){

    }


    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void setContentView(int layoutResID) {
        contentContainer.addView(inflateLayoutView(layoutResID));
    }

    public final View inflateLayoutView(int layoutResID){
        return LayoutInflater.from(this).inflate(layoutResID, contentContainer, false);
    }

    @Override
    public void setContentView(View view) {
        contentContainer.addView(view);
    }

    protected abstract void configureActionBar(ActionBar actionBar);


    protected final void attachProgressBar(@NonNull ProgressBar progressBar) {
        if (progressBar.isAttachedToWindow()) {
            return;
        }
        FrameLayout.LayoutParams frameLayoutParams;
        ViewGroup.LayoutParams layoutParams = progressBar.getLayoutParams();
        if (layoutParams instanceof FrameLayout.LayoutParams) {
            frameLayoutParams = (FrameLayout.LayoutParams) layoutParams;
        } else {
            frameLayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        frameLayoutParams.gravity = Gravity.CENTER;
        contentContainer.addView(progressBar, frameLayoutParams);
    }

    private void checkProgressBar(int maxProgress) {
        if (maxProgress != -1) {
            hideProgressBar();
            progressBar = createProgressBar(false);
            progressBar.setMax(maxProgress);
        } else {
            if (progressBar == null) {
                progressBar = createProgressBar(true);
            }
        }
    }

    @NonNull
    private ProgressBar createProgressBar(boolean isIndeterminate) {
        ProgressBar progressBar;
        if (isIndeterminate) {
            progressBar = new ProgressBar(this);
        } else {
            progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
        }
        progressBar.setIndeterminate(isIndeterminate);
        return progressBar;
    }

    protected final void showProgressBar() {
        if (isNonUsable()) {
            return;
        }
        checkProgressBar(-1);
        attachProgressBar(progressBar);
    }

    protected final void showProgressBar(int maxProgress) {
        if (isNonUsable()) {
            return;
        }
        checkProgressBar(maxProgress);
        attachProgressBar(progressBar);
    }

    protected final boolean applyBarProgress(int progress) {
        if (progressBar != null && !progressBar.isIndeterminate() && progressBar.isAttachedToWindow()) {
            progressBar.setProgress(progress);
            return true;
        }
        return false;
    }

    protected void hideProgressBar() {
        if (isNonUsable()) {
            return;
        }
        if (progressBar != null) {
            detachProgressBar(progressBar);
        }
    }

    protected final void detachProgressBar(@NonNull ProgressBar progressBar) {
        contentContainer.removeView(progressBar);
    }

    public final boolean isNonUsable() {
        return isFinishing() || isDestroyed();
    }

    protected final boolean isProgressBarShowing() {
        return progressBar != null && progressBar.isAttachedToWindow() && progressBar.getVisibility() == View.VISIBLE;
    }


}
