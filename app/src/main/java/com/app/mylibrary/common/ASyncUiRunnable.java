package com.app.mylibrary.common;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import java.lang.ref.SoftReference;

public abstract class ASyncUiRunnable<Argument, Result> implements Runnable {

    private SoftReference<IResult<Result>> resultSoftReference;
    private SoftReference<Argument> argumentSoftReference;

    public ASyncUiRunnable(Argument  argument, IResult<Result> iResult) {
        this.argumentSoftReference = new SoftReference<>(argument);
        this.resultSoftReference = new SoftReference<>(iResult);
    }

    @Nullable
    protected IResult<Result> getResultCallback() {
        if (resultSoftReference != null) {
            return resultSoftReference.get();
        }
        return null;
    }

    @Nullable
    protected Argument getArgument() {
        if (argumentSoftReference != null) {
            return argumentSoftReference.get();
        }
        return null;
    }

    protected final void throwResult(final Result result) {
        final IResult<Result> resultIResult = getResultCallback();
        if (resultIResult != null) {
            clear();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    resultIResult.onResult(result);
                }
            });
        }
    }

    private void clear(){
        resultSoftReference = null;
        argumentSoftReference = null;
    }

    public interface IResult<T> {
        void onResult(T result);
    }

}