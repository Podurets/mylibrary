package com.app.mylibrary.common;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

public class ApplicationStateWatcher implements Application.ActivityLifecycleCallbacks {

    private Activity activeActivity;
    private Activity aliveActivity;
    private int activeActivityCount;
    private int countCreated;
    private boolean isBackground = false;
    private AppStateListener appStateListener;

    public AppStateListener getAppStateListener() {
        return appStateListener;
    }

    public void setAppStateListener(AppStateListener appStateListener) {
        this.appStateListener = appStateListener;
    }

    public synchronized Activity getVisibleActivity() {
        return activeActivity;
    }

    public synchronized void resetActiveActivity() {
        activeActivity = null;
    }

    public synchronized Activity getAliveActivity() {
        return aliveActivity;
    }

    public synchronized void resetAliveActivity() {
        aliveActivity = null;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        aliveActivity = activity;
        countCreated++;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        activeActivity = activity;
        if (isBackground) {
            isBackground = false;
            if (appStateListener != null) {
                appStateListener.onGoToForeground();
            }
        }
        activeActivityCount++;
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        Activity lastVisibleActivity = getVisibleActivity();
        if (lastVisibleActivity != null && lastVisibleActivity.equals(activity)) {
            resetActiveActivity();
        }
        activeActivityCount--;
        if (activeActivityCount == 0 && countCreated > 0) {
            isBackground = true;
            if (appStateListener != null) {
                appStateListener.onGoToBackground();
            }
        }
    }

    public boolean isAnyActivityVisible() {
        return activeActivityCount > 0;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        countCreated--;
        Activity aliveActivity = getAliveActivity();
        if (aliveActivity != null && aliveActivity.equals(activity)) {
            resetAliveActivity();
        }
        if (countCreated == 0) {
            isBackground = false;
        }
    }

    public final boolean isAppInBackground() {
        return isBackground;
    }

    public interface AppStateListener {

        void onGoToForeground();

        void onGoToBackground();

    }

}