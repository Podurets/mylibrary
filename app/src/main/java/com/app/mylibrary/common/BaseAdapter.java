package com.app.mylibrary.common;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;


public abstract class BaseAdapter<T extends RecyclerView.ViewHolder>  extends RecyclerView.Adapter<T> {

    @NonNull
    protected final LayoutInflater inflater;
    protected PositionClickListener clickListener;

    public BaseAdapter(@NonNull Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setClickListener(PositionClickListener clickListener) {
        this.clickListener = clickListener;
    }

}
