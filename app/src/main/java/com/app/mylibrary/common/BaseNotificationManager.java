package com.app.mylibrary.common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

public abstract class BaseNotificationManager {

    @Nullable
    private static NotificationManager notificationManager;

    @Nullable
    public static NotificationManager getNotificationManager(@NonNull Context context) {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    private NotificationChannel verifyNotificationChannel(@NonNull Context context, @NonNull String channelId) {
        NotificationManager manager = getNotificationManager(context);
        if (manager == null) {
            return null;
        }
        NotificationChannelInfo channelInfo = getNotificationChannelInfo(channelId);
        if (channelInfo == null) {
            return null;
        }
        NotificationChannel channel = manager.getNotificationChannel(channelId);
        if (channel == null) {
            channel = onCreateChannelForSystem(channelInfo);
            manager.createNotificationChannel(channel);
        }
        return channel;
    }

    @Nullable
    protected final NotificationChannelInfo getNotificationChannelInfo(@NonNull String channelId) {
        NotificationChannelInfo[] channels = getChannels();
        int count = channels.length;
        for (int i = 0; i < count; i++) {
            if (channels[i].channelId.equals(channelId)) {
                return channels[i];
            }
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected final boolean isEnabledChannelInSystem(@NonNull Context context, @NonNull String channelId) {
        NotificationChannel channel = verifyNotificationChannel(context, channelId);
        return isEnabledChannelInSystem(channel);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected final boolean isEnabledChannelInSystem(NotificationChannel channel) {
        return channel != null && channel.getImportance() != NotificationManager.IMPORTANCE_NONE;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    protected final NotificationChannel getEnabledChannel(@NonNull Context context, @NonNull String channelId) {
        NotificationChannel channel = verifyNotificationChannel(context, channelId);
        if (isEnabledChannelInSystem(channel)) {
            return channel;
        }
        return null;
    }

    @NonNull
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected NotificationChannel onCreateChannelForSystem(@NonNull NotificationChannelInfo channelInfo) {
        return new NotificationChannel(channelInfo.channelId, channelInfo.channelName, channelInfo.defaultImportance);
    }

    protected void showNotification(@NonNull Context context, @NonNull NotificationInfo notificationInfo, @NonNull String channelId) {
        NotificationManager manager = getNotificationManager(context);
        if (manager == null) {
            return;
        }
        NotificationChannelInfo channelInfo = getNotificationChannelInfo(channelId);
        if (channelInfo == null) {
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = getEnabledChannel(context, channelId);
            if (channel != null) {
                Notification.Builder builder = onCreateNotification(context, notificationInfo, channel);
                builder.setContentIntent(onCreatePendingIntent(context, channelInfo, notificationInfo));
                manager.notify(channelInfo.notificationId, builder.build());
            }
        } else {
            NotificationCompat.Builder builder = onCreateCompatNotification(context, notificationInfo, channelId);
            builder.setContentIntent(onCreatePendingIntent(context, channelInfo, notificationInfo));
            manager.notify(channelInfo.notificationId, builder.build());
        }
    }

    protected NotificationCompat.Builder onCreateCompatNotification(@NonNull Context context, @NonNull NotificationInfo notificationInfo, @NonNull String channelId) {
        return new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(notificationInfo.resIconId)
                .setContentTitle(notificationInfo.title)
                .setContentText(notificationInfo.description);
    }


    @NonNull
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected Notification.Builder onCreateNotification(@NonNull Context context, @NonNull NotificationInfo notificationInfo, @NonNull NotificationChannel channel) {
        return new Notification.Builder(context, channel.getId())
                .setContentTitle(notificationInfo.title)
                .setContentText(notificationInfo.description)
                .setSmallIcon(notificationInfo.resIconId);
    }

    @NonNull
    private final NotificationChannelInfo[] channels;

    @NonNull
    public NotificationChannelInfo[] getChannels() {
        return channels;
    }

    protected BaseNotificationManager() {
        channels = initPossibleChannels();
    }

    @NonNull
    protected abstract NotificationChannelInfo[] initPossibleChannels();

    @Nullable
    protected abstract PendingIntent onCreatePendingIntent(@NonNull Context context, @NonNull NotificationChannelInfo notificationChannelInfo, @NonNull NotificationInfo notificationInfo);


    static final class NotificationChannelInfo {
        @NonNull
        public final String channelId;
        public final int notificationId;
        @NonNull
        public final String channelName;
        public int defaultImportance;

        public NotificationChannelInfo(@NonNull String channelId, int notificationId, @NonNull String channelName) {
            this.channelId = channelId;
            this.notificationId = notificationId;
            this.channelName = channelName;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setDefaultImportance(NotificationManager.IMPORTANCE_DEFAULT);
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        public void setDefaultImportance(int defaultImportance) {
            this.defaultImportance = defaultImportance;
        }
    }

    public final class NotificationInfo {
        @NonNull
        public final String title;
        @NonNull
        public final String description;
        @DrawableRes
        public final int resIconId;

        public NotificationInfo(@NonNull String title, @NonNull String description, int resIconId) {
            this.title = title;
            this.description = description;
            this.resIconId = resIconId;
        }
    }

}
