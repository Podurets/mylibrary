package com.app.mylibrary.common;


import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

public abstract class CancelableRunnable<T> implements Runnable {

    private volatile boolean isCanceled = false;
    @Nullable
    private volatile ResultCallback<T> resultCallback;

    public synchronized boolean isCanceled() {
        return isCanceled;
    }

    public synchronized void cancel() {
        isCanceled = true;
        destroy();
    }

    protected synchronized void destroy(){
        clearCallback();
    }

    public CancelableRunnable(@Nullable ResultCallback<T> resultCallback) {
        this.resultCallback = resultCallback;
    }

    protected synchronized void clearCallback(){
        resultCallback = null;
    }

    protected synchronized final ResultCallback<T> getResultCallback(){
        return resultCallback;
    }

    protected synchronized final boolean isCallbackAvailable() {
        return resultCallback != null;
    }

    protected synchronized void throwCallback(final T result) {
        //// throw callback
        if (!isCanceled()) {
            if (resultCallback != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        ResultCallback<T> localResultCallback = getResultCallback();
                        if (!isCanceled() && localResultCallback != null) {
                            localResultCallback.onResult(result);
                        }
                        destroy();
                    }
                });
            }
        } else {
            destroy();
        }
    }

}
