package com.app.mylibrary.common;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class CancelableThread<T> extends PriorityThread {

    @Nullable
    private CancelableRunnable<T> runnable;

    public synchronized void cancel() {
        if (runnable != null) {
            if (!runnable.isCanceled()) {
                runnable.cancel();
            }
            runnable = null;
        }
    }

    public CancelableThread(@NonNull CancelableRunnable<T> target) {
        super(target);
        runnable = target;
    }
}
