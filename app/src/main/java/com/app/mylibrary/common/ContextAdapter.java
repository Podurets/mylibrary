package com.app.mylibrary.common;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;

public abstract class ContextAdapter<T extends RecyclerView.ViewHolder> extends BaseAdapter<T> implements View.OnCreateContextMenuListener {

    private int contextMenuPosition = -1;
    @NonNull
    private Activity activity;

    public ContextAdapter(@NonNull Activity context) {
        super(context);
        activity = context;
    }

    public int getContextMenuPosition() {
        return contextMenuPosition;
    }


    @Override
    public void onBindViewHolder(final T holder, final int position) {
        bindHolder(holder, position);
        View view = getLongContextMenuTriggerView(holder, position);
        if (view != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contextMenuPosition = holder.getAdapterPosition();
                    view.showContextMenu();
                }
            });
            view.setOnCreateContextMenuListener(this);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        activity.getMenuInflater().inflate(getContextMenuRes(), menu);
    }


    public abstract void bindHolder(T viewHolder, int position);

    protected abstract View getLongContextMenuTriggerView(T viewHolder, int position);

    protected abstract int getContextMenuRes();

}
