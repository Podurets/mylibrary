package com.app.mylibrary.common;


import android.text.TextUtils;
import android.util.Log;

import com.app.mylibrary.BuildConfig;


public class DebugLogger {

    public static final boolean IS_LOG;

    static {
        IS_LOG = BuildConfig.DEBUG;
    }

    public static void e(String tag, String message) {

        if (IS_LOG && !TextUtils.isEmpty(message)) {
            Log.e(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

    public static void e(String tag, String message, Throwable e) {
        if (IS_LOG && !TextUtils.isEmpty(message)) {
            Log.e(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message, e);
        }
    }

    public static void w(String tag, String message) {
        if (IS_LOG && !TextUtils.isEmpty(message)) {
            Log.w(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

    public static void v(String tag, String message) {
        if (IS_LOG && !TextUtils.isEmpty(message)) {
            Log.v(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

    public static void i(String tag, String message) {
        if (IS_LOG && !TextUtils.isEmpty(message)) {
            Log.i(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

}
