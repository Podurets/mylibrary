package com.app.mylibrary.common;


import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;

import com.app.mylibrary.R;

public class NotificationsManager extends BaseNotificationManager {

    private static final String CONTACTS_SAVE_HISTORY_CHANNEL_ID = "con_ch";
    private static final int CONTACTS_SAVE_HISTORY_NOTIFICATION_ID = 10010;

    private static final String IMAGES_SAVE_HISTORY_CHANNEL_ID = "img_ch";
    private static final int IMAGES_SAVE_HISTORY_NOTIFICATION_ID = 10011;


    private static volatile NotificationsManager notificationManager;

    public static NotificationsManager getManager() {
        NotificationsManager localNotificationsManager = notificationManager;
        if (localNotificationsManager == null) {
            synchronized (NotificationsManager.class) {
                localNotificationsManager = notificationManager;
                if (localNotificationsManager == null) {
                    localNotificationsManager = notificationManager = new NotificationsManager();
                }
            }
        }
        return localNotificationsManager;
    }

    @NonNull
    @Override
    protected NotificationChannelInfo[] initPossibleChannels() {

        String contactsSaveToHistoryNotificationsChannelName = "TEST CHANNEL NAME";
        NotificationChannelInfo contactsSaveToHistoryNotificationChannelInfo =
                new NotificationChannelInfo(CONTACTS_SAVE_HISTORY_CHANNEL_ID, CONTACTS_SAVE_HISTORY_NOTIFICATION_ID, contactsSaveToHistoryNotificationsChannelName);

        String imagesSaveToHistoryNotificationsChannelName = "TEST CHANNEL NAME 2";
        NotificationChannelInfo imagesSaveToHistoryNotificationChannelInfo =
                new NotificationChannelInfo(IMAGES_SAVE_HISTORY_CHANNEL_ID, IMAGES_SAVE_HISTORY_NOTIFICATION_ID, imagesSaveToHistoryNotificationsChannelName);
        return new NotificationChannelInfo[]{contactsSaveToHistoryNotificationChannelInfo,
                imagesSaveToHistoryNotificationChannelInfo};
    }

    @Nullable
    @Override
    protected PendingIntent onCreatePendingIntent(@NonNull Context context, @NonNull NotificationChannelInfo notificationChannelInfo, @NonNull NotificationInfo notificationInfo) {
        // Creates an explicit intent for an Activity in your app
       // Intent resultIntent = new Intent(context, HistoryActivity.class);
// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
      //  stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
       // stackBuilder.addNextIntent(resultIntent);
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void showNotificationForImagesHistory(@NonNull Context context, @NonNull String fileName) {
        NotificationInfo notificationInfo = new NotificationInfo("Title test",
                "File " + fileName + " added to history.",
                R.mipmap.ic_launcher);
        showNotification(context, notificationInfo, IMAGES_SAVE_HISTORY_CHANNEL_ID);
    }

    public void showNotificationForContactsHistory(@NonNull Context context, @NonNull String fileName) {
        NotificationInfo notificationInfo = new NotificationInfo(context.getString(R.string.app_name),
                "File " + fileName + " added to history.",
                R.mipmap.ic_launcher);
        showNotification(context, notificationInfo, CONTACTS_SAVE_HISTORY_CHANNEL_ID);
    }


}
