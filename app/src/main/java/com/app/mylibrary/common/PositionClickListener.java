package com.app.mylibrary.common;

public interface PositionClickListener {

    void onClick(int position);
}
