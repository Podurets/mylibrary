package com.app.mylibrary.common;


import android.util.Log;

public class PriorityThread extends Thread {

    private static final int ADVANCED_PRIORITY = (MAX_PRIORITY + NORM_PRIORITY) / 2;
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    private static final int PROCESSORS_FOR_NORM_PRIORITY = 1;
    private static final int PROCESSORS_FOR_ADVANCED_PRIORITY = 2;

    public PriorityThread(Runnable target) {
        super(target);
        try {
            switch (NUMBER_OF_CORES) {
                case PROCESSORS_FOR_NORM_PRIORITY:
                    setPriority(NORM_PRIORITY);
                    break;
                case PROCESSORS_FOR_ADVANCED_PRIORITY:
                    setPriority(ADVANCED_PRIORITY);
                    break;
                default:
                    setPriority(MAX_PRIORITY);
            }
        } catch (Throwable e) {
            Log.e(PriorityThread.class.getSimpleName(), "Cannot modify thread priority", e);
        }
    }

    @Override
    public synchronized void start() {
        try {
            super.start();
        } catch (Throwable e) {
            Log.e(PriorityThread.class.getSimpleName(), "Cannot start thread", e);
        }
    }
}