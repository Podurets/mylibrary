package com.app.mylibrary.common;

public interface ResultCallback<T> {

   void onResult(T result);

}
