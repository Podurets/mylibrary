package com.app.mylibrary.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.mylibrary.common.PriorityThread;
import com.app.mylibrary.common.ResultCallback;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseDbHelper<T extends DbModel> {

    @NonNull
    protected abstract String getTableName();

    @NonNull
    protected abstract String getTableID();

    @Nullable
    protected abstract ContentValues getContentValues(@NonNull T instance);

    @Nullable
    protected abstract T fromCursor(@Nullable Cursor cursor);

    @NonNull
    protected abstract List<T> listFromCursor(@Nullable Cursor cursor);

    public void deleteAllRecords(@NonNull SQLiteDatabase db) {
        db.execSQL("delete from " + getTableName());
    }

    public void deleteRecord(@NonNull SQLiteDatabase db, int recordId) {
        try {
            db.delete(getTableName(), getTableID() + "=?", new String[]{String.valueOf(recordId)});
        } catch (Throwable e) {
            Log.e(BaseDbHelper.class.getSimpleName(), "deleteRecord", e);
        }
    }
// TODO: 13.12.2017 implement it
   /* public void createOrUpdate(@NonNull final T instance, ResultCallback<Boolean> resultCallback) {
        new PriorityThread(new DbRunnable<Boolean>(resultCallback) {
            @Override
            public void run() {
                Boolean result = createOrUpdate(SqLiteHelper.getHelper().getWritableDbIgnoreLocker(), instance);
                throwResult(result);
            }
        }).start();
    }*/

    public Boolean createOrUpdate(@NonNull SQLiteDatabase db, @NonNull T instance) {
        ContentValues values = getContentValues(instance);
        if (values != null) {
            long id = instance.getRecordId();
            if (id > 0) {
               return update(db, values, id);
            } else {
                id = create(db, values);
                instance.setRecordId(id);
                return id > -1;
            }
        }
        return Boolean.FALSE;
    }

    private long create(@NonNull SQLiteDatabase db, @NonNull ContentValues values) {
        try {
           return db.insertOrThrow(getTableName(), null, values);
        } catch (Throwable e) {
            Log.e(BaseDbHelper.class.getSimpleName(), "create", e);
        }
        return -1;
    }

    private Boolean update(@NonNull SQLiteDatabase db, @NonNull ContentValues values, long recordId) {
        try {
            return db.update(getTableName(), values, getTableID() + "=?", new String[]{String.valueOf(recordId)}) > 0;
        } catch (Throwable e) {
            Log.e(BaseDbHelper.class.getSimpleName(), "update", e);
        }
        return Boolean.FALSE;
    }

    @Nullable
    protected T getRecord(@NonNull SQLiteDatabase db, int recordId) {
        Cursor cursor = null;
        try {
            cursor = db.query(getTableName(), null, getTableID() + "=?", new String[]{String.valueOf(recordId)}, null, null, null);
            return fromCursor(cursor);
        } catch (Throwable e) {
            Log.e(BaseDbHelper.class.getSimpleName(), "getRecord", e);
        } finally {
            // TODO: 13.12.2017 close cursor
          //  SqLiteHelper.closeCursor(cursor);
        }
        return null;
    }

    @NonNull
    protected List<T> getAllRecords(@NonNull SQLiteDatabase db) {
        Cursor cursor = null;
        try {
            cursor = db.query(getTableName(), null, null, null, null, null, null);
            return listFromCursor(cursor);
        } catch (Throwable e) {
            Log.e(BaseDbHelper.class.getSimpleName(), "getAllRecords", e);
        } finally {
            // TODO: 13.12.2017 close cursor
            //SqLiteHelper.closeCursor(cursor);
        }
        return new ArrayList<>();
    }

    public void getAllRecords(@Nullable ResultCallback<List<T>> resultCallback) {
        if (resultCallback != null) {
            // TODO: 13.12.2017 implement it
           /* new PriorityThread(new DbRunnable<List<T>>(resultCallback) {
                @Override
                public void run() {
                    List<T> result = getAllRecords(SqLiteHelper.getHelper().getReadableDbIgnoreLocker());
                    throwResult(result);
                }
            }).start();*/
        }
    }

}
