package com.app.mylibrary.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;

public class CredentialsManager {

    private static volatile CredentialsManager instanceCredentialsManager;

    private static final String USER_NAME = "user_key";
    private static final String PASSWORD = "pass_key";
    private static final String PREF_FILE_NAME = "session_key";

    public static CredentialsManager getCredentialsManager() {
        CredentialsManager localCredentialsManager = instanceCredentialsManager;
        if (localCredentialsManager == null) {
            synchronized (CredentialsManager.class) {
                localCredentialsManager = instanceCredentialsManager;
                if (localCredentialsManager == null) {
                    localCredentialsManager = instanceCredentialsManager = new CredentialsManager();
                }
            }
        }
        return localCredentialsManager;
    }

    @NonNull
    private static String encrypt(@NonNull String input) {
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    @NonNull
    private static String decrypt(@NonNull String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

    @NonNull
    private SharedPreferences getSharedPreferences(@NonNull Context context) {
        return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void saveValue(@NonNull Context context,@NonNull String key, @Nullable String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        if (TextUtils.isEmpty(value)) {
            editor.remove(encrypt(key));
        } else {
            editor.putString(encrypt(key), encrypt(value));
        }
        editor.apply();
    }

    @Nullable
    public String getValue(@NonNull Context context, @NonNull String key) {
        SharedPreferences preferences = getSharedPreferences(context);
        String value = preferences.getString(encrypt(key), null);
        if (value != null) {
            value = decrypt(value);
        }
        return value;
    }
}
