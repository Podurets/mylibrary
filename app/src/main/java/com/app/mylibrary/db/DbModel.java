package com.app.mylibrary.db;


public class DbModel {

    private volatile long recordId;

    public long getRecordId() {
        return recordId;
    }

    public void setRecordId(long recordId) {
        this.recordId = recordId;
    }
}
