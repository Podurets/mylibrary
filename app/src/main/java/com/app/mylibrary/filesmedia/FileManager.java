package com.app.mylibrary.filesmedia;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import com.app.mylibrary.common.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileManager {

    public static final int FILE_BUFFER_SIZE = 4096;

    private String getTempImageName() {
        return "image_" + System.currentTimeMillis();
    }

    public File getTempImageFile(@NonNull Context context) throws IOException {
        File storageDir = getTempStorageDir(context);
        return File.createTempFile(getTempImageName(), ".jpg", storageDir);
    }

    public static File getTempStorageDir(@NonNull Context context){
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public static File getPicassoCacheDirectory(@NonNull Context context) {
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir == null) {
            cacheDir = context.getCacheDir();
        }
        if (cacheDir != null) {
            cacheDir = new File(cacheDir, "picassoCache");
            if (!cacheDir.exists() && !cacheDir.mkdir()) {
                return null;
            }
        }
        return cacheDir;
    }

    public static long getOptimalBufferPicassoSize(File picassoCachePath) {
        if (picassoCachePath != null) {
            long freeSpace = picassoCachePath.getFreeSpace();
            long totalSpace = picassoCachePath.getTotalSpace();
            //// 524288000 - 500 Mb
            if (totalSpace > 524288000) {
                /// 524288000 - 500 Mb
                if (freeSpace > 524288000) {
                    /// 314572800 - 300 Mb
                    return 314572800;
                } else {
                    /// 52428800 - 50 Mb
                    if (freeSpace > 52428800) {
                        return 52428800;
                    } else {
                        return freeSpace;
                    }
                }
            } else {
                /// 104857600 - 100 Mb
                if (freeSpace > 104857600) {
                    return 104857600;
                } else {
                    /// 52428800 - 50 Mb
                    if (freeSpace > 52428800) {
                        return 52428800;
                    } else {
                        return freeSpace;
                    }
                }
            }
        }
        return 0;
    }

    public static Uri getFileUri(@NonNull Context context,  File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(context,
                   // context.getString(R.string.app_authority),
                    "authority",
                    file);
        }
        return Uri.fromFile(file);
    }

    public static synchronized File saveTemporaryFile(@NonNull Context context, InputStream fileStream, String fileName){
        File storageDir = getTempStorageDir(context);
        OutputStream output = null;
        try {
            File file = new File(storageDir, fileName);
            output = new FileOutputStream(file);
            try {
                byte[] buffer = new byte[FILE_BUFFER_SIZE];
                int read;
                while ((read = fileStream.read(buffer)) != Constants.INVALID_CODE) {
                    output.write(buffer, 0, read);
                }
                output.flush();
                return file;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            FileManager.closeStream(output);
            FileManager.closeStream(fileStream);
        }
        return null;
    }

    @NonNull
    public static File getAppCacheDir(@NonNull Context context,
                                      @NonNull String dir) {
        return createDefaultCacheDir(context, dir);
    }

    @NonNull
    public static File createDefaultCacheDir(@NonNull Context context,
                                             @NonNull String path) {

        File cache = context.getExternalCacheDir();
        if (cache == null) {
            cache = context.getCacheDir();
        }

        if (!TextUtils.isEmpty(path)) {
            cache = new File(cache, path);
        }

        if (!cache.exists()) {
            //noinspection ResultOfMethodCallIgnored
            cache.mkdirs();
        }
        return cache;
    }

    public static void createFolderIfNotExists(@NonNull File dir) throws FileNotFoundException {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new FileNotFoundException("Directory not created");
        }
    }

    public static synchronized void closeStream(InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Throwable e) {
                Log.e(FileManager.class.getSimpleName(), "closeStream");
            }
        }
    }

    public static synchronized void closeStream(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Throwable e) {
                Log.e(FileManager.class.getSimpleName(), "close OutputStream");
            }
        }
    }
}
