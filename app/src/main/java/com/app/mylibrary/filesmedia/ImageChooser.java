package com.app.mylibrary.filesmedia;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.app.mylibrary.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageChooser {

    public static final int CHOICE_GALLERY = 0;
    public static final int CHOICE_CAMERA = 1;

    public static final int RESULT_GALLERY = 1011;
    public static final int RESULT_CAMERA = 1012;
    public static final int ASK_STORAGE_IMAGES_PERMISSION_REQUEST_CODE = 3012;

    private static List<String> acceptedImageExtensions;
    private static String[] imageMimeTypes;

    private Activity activity;
    private int actionCode;
    private Uri imageUri;
    private FileManager fileManager = new FileManager();
    private final MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
    private UriCallback uriCallback;

    public ImageChooser(@NonNull Activity activity, UriCallback callback) {
        this.activity = activity;
        uriCallback = callback;
    }

    private void loadChoosePictureAccordingToAction() {
        imageUri = null;
        switch (actionCode) {
            case CHOICE_GALLERY:
                showGallery(activity);
                break;
            case CHOICE_CAMERA:
                try {
                    File tempImageFile = fileManager.getTempImageFile(activity);
                    imageUri = FileManager.getFileUri(activity, tempImageFile);
                    getPhotoFromCamera(activity, imageUri);
                } catch (Throwable e) {
                    Log.e(ImageChooser.class.getSimpleName(), "getPhotoFromCamera", e);
                    //App.showToast(R.string.error_access_camera);
                }
                break;
        }
    }

    private void showGallery(@NonNull Activity activity) {
        Intent intent = new Intent();
        intent.setType("image/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, getImageMimeTypes());
        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent,
                activity.getString(R.string.dialog_choose_image)),
                RESULT_GALLERY);
    }

    private static void getPhotoFromCamera(@NonNull Activity activity, Uri uri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (uri != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(intent, RESULT_CAMERA);
        } else {
            //App.showToast(activity.getString(R.string.error_access_camera));
        }
    }

    public void showChoosePictureDialog() {
        if (activity != null) {
            Resources res = activity.getResources();
            AlertDialog.Builder builder = new AlertDialog.Builder(activity/*, R.style.AppCompatAlertDialogStyle*/);
            builder.setTitle(R.string.dialog_choose_picture);
            String[] items = res.getStringArray(R.array.choose_picture_dialog_items);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (ContextCompat.checkSelfPermission(activity,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(activity,
                                Manifest.permission.MANAGE_DOCUMENTS) == PackageManager.PERMISSION_GRANTED) {
                            optionSelected(which, true);
                        } else {
                            optionSelected(which, false);
                        }
                    } else {
                        optionSelected(which, true);
                    }
                }
            });
            builder.setCancelable(true);
            builder.show();
        }
    }

    private void optionSelected(int option, boolean isPermissionsGranted) {
        actionCode = option;
        if (activity == null) {
            return;
        }
        if (isPermissionsGranted) {
            loadChoosePictureAccordingToAction();
        } else {
            String[] permissions;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.MANAGE_DOCUMENTS};
            } else {
                permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
            }
            ActivityCompat.requestPermissions(activity, permissions,
                    ASK_STORAGE_IMAGES_PERMISSION_REQUEST_CODE);
        }
    }

    private final void notifyImageResourceChanged() {
        if (uriCallback != null && imageUri != null) {
            uriCallback.onUriPrepared(imageUri);
        } else {
            Log.w(ImageChooser.class.getSimpleName(), "notifyImageResourceChanged uriCallback is Null");
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                switch (requestCode) {
                    case RESULT_GALLERY:
                        if (data != null) {
                            imageUri = data.getData();
                        }
                    case RESULT_CAMERA:
                        if (checkUploadFileMimeType(imageUri)) {
                            notifyImageResourceChanged();
                        } else {
                            String imageUriMessage;
                            if (imageUri == null) {
                                imageUriMessage = "imageUri from camera is null";
                                Log.e(ImageChooser.class.getSimpleName(), imageUriMessage);
                                return;
                            }
                            imageUriMessage = "wrong mime type for uri ";
                            Log.e(ImageChooser.class.getSimpleName(), imageUriMessage + imageUri);
                        }
                        break;
                }
                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, int[] grantResults) {
        switch (requestCode) {
            case ASK_STORAGE_IMAGES_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadChoosePictureAccordingToAction();
                } else {
                  //  App.showToast(R.string.error_access_storage);
                }
            }
            break;
        }
    }

    private boolean checkUploadFileMimeType(Uri uri) {
        if (uri != null && activity != null) {
            String extension = mimeTypeMap.getExtensionFromMimeType(activity.getContentResolver().getType(uri));
            if (extension == null) {
                extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
            }
            if (extension == null) {
                extension = "";
            }
            if (!getAcceptedImageExtensions().contains(extension.toLowerCase())) {
                //App.showToast(R.string.error_wrong_image_type);
                return false;
            }
            return true;
        }
        return false;
    }

    public final void release() {
        activity = null;
        imageUri = null;
        uriCallback = null;
    }

    private String[] getImageMimeTypes() {
        if (imageMimeTypes == null) {
            imageMimeTypes = new String[]{"image/jpeg", "image/gif", "image/jpg", "image/png"};
        }
        return imageMimeTypes;
    }

    private List<String> getAcceptedImageExtensions() {
        if (acceptedImageExtensions == null) {
            acceptedImageExtensions = new ArrayList<>();
            String[] acceptedMimeTypes = getImageMimeTypes();
            for (String mime : acceptedMimeTypes) {
                String extension = mimeTypeMap.getExtensionFromMimeType(mime);
                if (extension != null) {
                    acceptedImageExtensions.add(extension);
                }
            }
        }
        return acceptedImageExtensions;
    }

   public interface UriCallback {
        void onUriPrepared(Uri uri);
    }

}
