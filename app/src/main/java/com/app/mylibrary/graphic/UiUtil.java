package com.app.mylibrary.graphic;

import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.widget.TextView;

public class UiUtil {

    public static void setTextColor(@NonNull TextView textView, @ColorInt int color) {
        if (textView.getCurrentTextColor() != color) {
            textView.setTextColor(color);
        }
    }

}
