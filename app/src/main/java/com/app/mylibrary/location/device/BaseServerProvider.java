package com.app.mylibrary.location.device;


import okhttp3.OkHttpClient;

public class BaseServerProvider<Response>  extends Loader<Response> {

    private static OkHttpClient client = new OkHttpClient();


    public static OkHttpClient getClient() {
        return client;
    }

    public static boolean isSuccessRequest(int statusCode){
        return statusCode >= 200 && statusCode < 300;
    }

}
