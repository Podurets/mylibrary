package com.app.mylibrary.location.device;

import android.content.Context;
import android.support.annotation.NonNull;


import com.app.mylibrary.common.CancelableRunnable;
import com.app.mylibrary.common.CancelableThread;
import com.app.mylibrary.common.ResultCallback;

import java.lang.ref.SoftReference;

public class DeviceInfoLoader extends Loader<DeviceNetworkInfo> {

    private SoftReference<Context> softReferenceContext;

    public DeviceInfoLoader(@NonNull Context context) {
        this.softReferenceContext = new SoftReference<>(context);
    }

    private Context getContext() {
        if (softReferenceContext != null) {
            return softReferenceContext.get();
        }
        return null;
    }

    public void loadDeviceInfo(@NonNull ResultCallback<DeviceNetworkInfo> callback) {
        startWork(new CancelableThread<>(new CancelableRunnable<DeviceNetworkInfo>(callback) {
            @Override
            public void run() {
                if (isCanceled()) {
                    return;
                }
                if (!isCallbackAvailable()) {
                    return;
                }

                IpProvider.ProviderResult providerResult = new DeviceInfoManager().getDeviceInfo(getContext());
                DeviceNetworkInfo deviceNetworkInfo = providerResult.getNetworkInfo();
                throwCallback(deviceNetworkInfo);
            }
        }));
    }

}
