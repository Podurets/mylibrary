package com.app.mylibrary.location.device;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.app.mylibrary.common.DebugLogger;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class DeviceInfoManager {

    private static final ConcurrentHashMap<String, Integer> requestInfo = new ConcurrentHashMap<>();

    @NonNull
    public IpProvider.ProviderResult getDeviceInfo(Context context) {
        /*if (System.currentTimeMillis() % 2 == 0) {
            return new IpApiProvider(context).getIpResult();
        } else {
            return new IpnfProvider(context).getIpResult();
        }*/
        //return new IpApiCoProvider(context).getIpResult();
        return getIpProviderByRequests(context).getIpResult();
    }

    private int getCountByKey(@NonNull String key) {
        Integer count = requestInfo.get(key);
        return count == null ? 0 : count;
    }

    private IpProvider returnProvider(@NonNull IpProvider provider, int oldProviderCallsCount) {
        String name = provider.getClass().getSimpleName();
        requestInfo.put(name, ++oldProviderCallsCount);
        DebugLogger.e(DeviceInfoManager.class.getSimpleName(), "updated provider:" + name + ", with calls: " + getCountByKey(name));
        return provider;
    }

    @NonNull
    private synchronized IpProvider getIpProviderByRequests(@NonNull Context context) {
        int countIpApiProvider = getCountByKey(IpApiProvider.class.getSimpleName());
        int countIpnfProvider = getCountByKey(IpnfProvider.class.getSimpleName());
        int countIpApiCoProvider = getCountByKey(IpApiCoProvider.class.getSimpleName());
        if (countIpApiProvider == 0) {
            return returnProvider(new IpApiProvider(context), countIpApiProvider);
        }
        if (countIpnfProvider == 0) {
            return returnProvider(new IpnfProvider(context), countIpnfProvider);
        }
        if (countIpApiCoProvider == 0) {
            return returnProvider(new IpApiCoProvider(context), countIpApiCoProvider);
        }
        if (countIpApiProvider > countIpnfProvider) {
            if(countIpnfProvider > countIpApiCoProvider){
                return returnProvider(new IpApiCoProvider(context), countIpApiCoProvider);
            }
            return returnProvider(new IpnfProvider(context), countIpnfProvider);
        }
        return returnProvider(new IpApiProvider(context), countIpApiProvider);
    }

}
