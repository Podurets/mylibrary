package com.app.mylibrary.location.device;

import android.content.Context;

public class DeviceNetworkInfo {

    private double latitude;
    private double longitude;
    private long obtainTime;

    public DeviceNetworkInfo(double latitude, double longitude, long obtainTime) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.obtainTime = obtainTime;
    }

    public DeviceNetworkInfo(long obtainTime) {
        this.obtainTime = obtainTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getObtainTime() {
        return obtainTime;
    }

    public void setObtainTime(long obtainTime) {
        this.obtainTime = obtainTime;
    }

    // FIXME: 29.10.2017  implement normal initUnknown method
    public static DeviceNetworkInfo initUnknown() {
        return new DeviceNetworkInfo(49.4181207, 32.0690918, System.currentTimeMillis());
    }

    // FIXME: 29.10.2017  implement normal initDefault method
    public static DeviceNetworkInfo initDefault(Context context) {
        return initUnknown();
    }
}
