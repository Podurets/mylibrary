package com.app.mylibrary.location.device;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

public class IpApiCoProvider extends IpProvider {

    public IpApiCoProvider(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    protected ProviderResult getNetworkIpResult() {
        Request request = new Request.Builder().url("https://ipapi.co/json/").build();
        try {
            Response response = client.newCall(request).execute();
            String stringResponse = response.body().string();
            if (!TextUtils.isEmpty(stringResponse)) {
                return parseJsonResponse(stringResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    @Override
    protected String getTestJsonResponse() {
        return "{\n" +
                "    \"ip\": \"78.137.7.91\",\n" +
                "    \"city\": \"Cherkasy\",\n" +
                "    \"region\": \"Cherkas'ka Oblast'\",\n" +
                "    \"region_code\": \"71\",\n" +
                "    \"country\": \"UA\",\n" +
                "    \"country_name\": \"Ukraine\",\n" +
                "    \"postal\": null,\n" +
                "    \"latitude\": 49.4285,\n" +
                "    \"longitude\": 32.0621,\n" +
                "    \"timezone\": \"Europe/Kiev\",\n" +
                "    \"asn\": \"AS25133\",\n" +
                "    \"org\": \"LLC McLaut-Invest\"\n" +
                "}";
    }

    @Nullable
    @Override
    protected ProviderResult parseJsonResponse(@NonNull String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            double latitude = jsonObject.getDouble("latitude");
            double longitude = jsonObject.getDouble("longitude");
            if (latitude != 0 && longitude != 0) {
                DeviceNetworkInfo networkInfo = new DeviceNetworkInfo(System.currentTimeMillis());
                networkInfo.setLongitude(longitude);
                networkInfo.setLatitude(latitude);
                return new ProviderResult(networkInfo, 200);
            } else {
                Log.e(IpApiCoProvider.class.getSimpleName(), "parseJsonResponse ip empty for response: " + response);
            }
        } catch (JSONException e) {
            Log.e(IpApiCoProvider.class.getSimpleName(), "parseJsonResponse", e);
        }
        return null;
    }
}
