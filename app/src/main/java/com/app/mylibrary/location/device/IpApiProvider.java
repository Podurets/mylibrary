package com.app.mylibrary.location.device;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

public class IpApiProvider extends IpProvider {


    public IpApiProvider(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    protected ProviderResult getNetworkIpResult() {
        Request request = new Request.Builder().url("http://ip-api.com/json").build();
        try {
            Response response = client.newCall(request).execute();
            String stringResponse = response.body().string();
            if (!TextUtils.isEmpty(stringResponse)) {
                return parseJsonResponse(stringResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    @Override
    protected String getTestJsonResponse() {
        return "{\"as\":\"AS25133 LLC McLaut-Invest\",\"city\":\"Cherkasy\",\"country\":\"Ukraine\",\"countryCode\":\"UA\",\"isp\":\"LLC McLaut-Invest\",\"lat\":49.4285,\"lon\":32.0621,\"org\":\"McLaut ISP\",\"query\":\"78.137.7.91\",\"region\":\"71\",\"regionName\":\"Cherkas'ka Oblast'\",\"status\":\"success\",\"timezone\":\"Europe/Kiev\",\"zip\":\"\"}";
    }

    @Nullable
    @Override
    protected ProviderResult parseJsonResponse(@NonNull String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            double lat = jsonObject.getDouble("lat");
            double lon = jsonObject.getDouble("lon");
            if (lat != 0 && lon != 0) {
                DeviceNetworkInfo networkInfo = new DeviceNetworkInfo(System.currentTimeMillis());
                networkInfo.setLatitude(lat);
                networkInfo.setLongitude(lon);
                //return new ProviderResult(networkInfo, 200);
            }
        } catch (JSONException e) {
            Log.e(IpApiProvider.class.getSimpleName(), "parseJsonResponse", e);
        }
        return null;
    }
}
