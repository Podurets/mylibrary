package com.app.mylibrary.location.device;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.mylibrary.common.DebugLogger;

import java.lang.ref.SoftReference;

import okhttp3.OkHttpClient;

public abstract class IpProvider {

    protected final OkHttpClient client = new OkHttpClient();
    private SoftReference<Context> contextSoftReference;

    public static final boolean isUseCachedNetworkData = false;

    @Nullable
    protected abstract ProviderResult getNetworkIpResult();
    @NonNull
    protected abstract String getTestJsonResponse();
    @Nullable
    protected abstract ProviderResult parseJsonResponse(@NonNull String response);

    public IpProvider(@NonNull Context context) {
        this.contextSoftReference = new SoftReference<>(context);
    }

    @Nullable
    public Context getContext() {
        if (contextSoftReference != null) {
            return contextSoftReference.get();
        }
        return null;
    }

    @NonNull
    public ProviderResult getIpResult(){
        ProviderResult providerResult = null;
        try {
            if (isUseCachedNetworkData) {
                providerResult = getTestJsonIpResult();
            } else {
                providerResult = getNetworkIpResult();
            }
        } catch (Exception e) {
            DebugLogger.e(getClass().getSimpleName(), "getIpResult", e);
        } finally {
            if (providerResult == null) {
                providerResult = new ProviderResult(getLocalDeviceInfo(), 200);
            }
        }
        return providerResult;
    }

    @Nullable
    protected ProviderResult getTestJsonIpResult() {
        return parseJsonResponse(getTestJsonResponse());
    }

    public static class ProviderResult {

        private DeviceNetworkInfo networkInfo;
        private int status;

        public ProviderResult(DeviceNetworkInfo networkInfo, int status) {
            this.networkInfo = networkInfo;
            this.status = status;
        }

        public DeviceNetworkInfo getNetworkInfo() {
            return networkInfo;
        }

        public void setNetworkInfo(DeviceNetworkInfo networkInfo) {
            this.networkInfo = networkInfo;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }

    protected DeviceNetworkInfo getLocalDeviceInfo() {
        Context context = getContext();
        if (context == null) {
            return DeviceNetworkInfo.initUnknown();
        } else {
            return DeviceNetworkInfo.initDefault(context);
        }
    }

}
