package com.app.mylibrary.location.device;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

public class IpnfProvider extends IpProvider {

    public IpnfProvider(Context context) {
        super(context);
    }

    @Nullable
    @Override
    public ProviderResult getNetworkIpResult() {
        Request request = new Request.Builder().url("http://ipv4.ip.nf/me.json").build();
        try {
            Response response = client.newCall(request).execute();
            String stringResponse = response.body().string();
            if (!TextUtils.isEmpty(stringResponse)) {
                return parseJsonResponse(stringResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @NonNull
    @Override
    protected String getTestJsonResponse() {
        return "{\"ip\":{\"ip\":\"78.137.7.91\",\"asn\":\"AS25133 LLC McLaut-Invest\",\"netmask\":18,\"hostname\":\"78-137-7-91.static-pool.mclaut.net.\",\"city\":\"Cherkassy\",\"post_code\":\"\",\"country\":\"Ukraine\",\"country_code\":\"UA\",\"latitude\":49.42850112915039,\"longitude\":32.06209945678711}}";
    }

    @Nullable
    @Override
    protected ProviderResult parseJsonResponse(@NonNull String response) {
        try {
            JSONObject responseJson = new JSONObject(response);
            JSONObject ipJSONObject = responseJson.getJSONObject("ip");
            double latitude = ipJSONObject.getDouble("latitude");
            double longitude = ipJSONObject.getDouble("longitude");
            if (latitude != 0 && longitude != 0) {
                DeviceNetworkInfo deviceNetworkInfo = new DeviceNetworkInfo(System.currentTimeMillis());
                deviceNetworkInfo.setLatitude(latitude);
                deviceNetworkInfo.setLongitude(longitude);
                // return new ProviderResult(deviceNetworkInfo, 200);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
