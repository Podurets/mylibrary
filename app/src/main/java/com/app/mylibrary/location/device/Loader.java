package com.app.mylibrary.location.device;

import android.support.annotation.NonNull;

import com.app.mylibrary.common.CancelableThread;


public class Loader<T> {

    private volatile CancelableThread<T> cancelableThread;

    public Loader() {

    }

    public boolean isWorking() {
        return cancelableThread != null && cancelableThread.isAlive();
    }

    public void cancelWork() {
        if (cancelableThread != null) {
            cancelableThread.cancel();
            cancelableThread = null;
        }
    }

    protected void startWork(@NonNull CancelableThread<T> newWorkThread){
        cancelWork();
        newWorkThread.start();
        cancelableThread = newWorkThread;
    }

}
