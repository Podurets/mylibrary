package com.app.mylibrary.net;

public interface ICallback<Data> {

    void onSuccess(Data data);
    void onError(String message);

}
