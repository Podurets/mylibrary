package com.app.mylibrary.net;

import android.support.annotation.Nullable;

import java.lang.ref.SoftReference;

import okhttp3.Callback;

public abstract class OkhttpCalback<Data> implements Callback {

    private SoftReference<ICallback<Data>> callbackSoftReference;

    public OkhttpCalback(ICallback<Data> serverCallback) {
        this.callbackSoftReference = new SoftReference<>(serverCallback);
    }

    @Nullable
    private ICallback<Data> getServerCallback() {
        return callbackSoftReference != null ? callbackSoftReference.get() : null;
    }

    protected void throwSuccess(Data data) {
        ICallback<Data> callback = getServerCallback();
        if (callback != null) {
            clearCallback();
            callback.onSuccess(data);
        }
    }

    protected void throwError(String message) {
        ICallback<Data> callback = getServerCallback();
        if (callback != null) {
            clearCallback();
            callback.onError(message);
        }
    }

    private void clearCallback() {
        callbackSoftReference = null;
    }

}
